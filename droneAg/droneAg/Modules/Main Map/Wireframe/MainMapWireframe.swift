//
//  MainMapWireframe.swift
//  droneAg
//
//  Created by Nikolaus Banjo 07729431578 on 21/03/2022.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//
import UIKit

class MainMapWireframe:  MainMapWireframeProtocol {
    static func createMainMapModule() -> UIViewController {
        guard let view = UIStoryboard(name: "MainMapView", bundle: nil).instantiateViewController(withIdentifier: "MainMapView") as? MainMapView else {
            return UIViewController()
        }
        let presenter: MainMapPresenterProtocol & MainMapInteractorOutputProtocol = MainMapPresenter()
        let interactor: MainMapInteractorInputProtocol = MainMapInteractor()
        let wireframe: MainMapWireframeProtocol = MainMapWireframe()
        
        view.presenter = presenter
        presenter.view = view
        presenter.interactor = interactor
        presenter.wireframe = wireframe
        interactor.presenter = presenter
        
        return view
    }
    
}
