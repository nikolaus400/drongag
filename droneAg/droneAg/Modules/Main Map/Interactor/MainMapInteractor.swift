//
//  MainMapInteractor.swift
//  droneAg
//
//  Created by Nikolaus Banjo 07729431578 on 21/03/2022.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//


import CoreData

class MainMapInteractor: MainMapInteractorInputProtocol {
    weak var presenter: MainMapInteractorOutputProtocol?
    
    lazy var fetchedResultsController: NSFetchedResultsController<Polygon> = {
       let controller = NSFetchedResultsController(fetchRequest: Polygon.getFetchRequest(),
                                                   managedObjectContext: CoreDataServices.shared.context,
                                                   sectionNameKeyPath: nil, cacheName: nil)

       return controller
   }()
    
    func getPolygons() {
        do {
            try fetchedResultsController.performFetch()
        } catch {
            let nserror = error as NSError
            fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
        }
        
        if let polygons = fetchedResultsController.fetchedObjects,
           !polygons.isEmpty {
            presenter?.didGetPolygons(polygons: polygons)
        }
        
    }
    
    func savePolygon(pathLatitudeLongitude: [(Double,Double)], area: Double, centreLatitudeLongitude: (Double,Double)) {
        let coreDataEditedPolygon = Polygon.create(context: CoreDataServices.shared.context)
        
        for (latitude,longitude) in pathLatitudeLongitude {
            let coreDataCoordinate = Coordinate.create(context: CoreDataServices.shared.context)
            coreDataCoordinate.latitude = NSNumber.init(value: latitude)
            coreDataCoordinate.longitude = NSNumber.init(value: longitude)
            coreDataEditedPolygon.addToCoordinates(coreDataCoordinate)
        }
        
        coreDataEditedPolygon.area = NSNumber.init(value: area)
        
        let centreCoordinate = Coordinate.create(context: CoreDataServices.shared.context)
        centreCoordinate.latitude = NSNumber.init(value: centreLatitudeLongitude.0)
        centreCoordinate.longitude = NSNumber.init(value: centreLatitudeLongitude.1)
        coreDataEditedPolygon.centreCoordinate = centreCoordinate
        
        CoreDataServices.shared.save(context: CoreDataServices.shared.context)
    }
}
