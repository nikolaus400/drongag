//
//  MainMapView.swift
//  droneAg
//
//  Created by Nikolaus Banjo 07729431578 on 21/03/2022.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.

import UIKit
import GoogleMaps
import CoreData

class MainMapView: UIViewController {
    
    //MARK: - Variables
    var presenter: MainMapPresenterProtocol?
    
    var locationManager: CLLocationManager!
    var mapView: GMSMapView!
    var preciseLocationZoomLevel: Float = 15.0
    var approximateLocationZoomLevel: Float = 10.0
    var defaultLocationLatitude = -33.869405
    var defaultLocationLongitude = 151.199
    var editedPath = GMSMutablePath()
    var editedPolygon = GMSPolygon()
    
    
    //MARK: - Outlets
    @IBOutlet var mapContainerView: UIView!
    @IBOutlet var savePolygonButton: UIButton!
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    func setup() {
        setupMap()
        savePolygonButton.isHidden = true
        presenter?.getPolygons()
    }
    
    func setupMap() {
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.distanceFilter = 50
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
        
        let defaultLocation = CLLocation(latitude: defaultLocationLatitude, longitude: defaultLocationLongitude)// A default location to use when location permission is not granted.
        
        // Create a map.
        let zoomLevel = locationManager.accuracyAuthorization == .fullAccuracy ? preciseLocationZoomLevel : approximateLocationZoomLevel
        let camera = GMSCameraPosition.camera(withLatitude: defaultLocation.coordinate.latitude,
                                              longitude: defaultLocation.coordinate.longitude,
                                              zoom: zoomLevel)
        mapView = GMSMapView.map(withFrame: view.bounds, camera: camera)
        mapView.delegate = self
        mapView.settings.myLocationButton = true
        
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView.isMyLocationEnabled = true

        // Add the map to the view
        mapContainerView.addSubview(mapView)
        mapView.isHidden = true
    }
}

extension MainMapView {
    //MARK: - IBActions
    @IBAction func savePolygonButtonPressed(_ sender: Any) {
        
        savePolygonButton.isHidden = true
        
        //collect path data for saving
        var rawPath = [(Double,Double)]()
        
        for x in 0..<editedPath.count() {
            let googleCoordinate = editedPath.coordinate(at: x)
            rawPath.append((googleCoordinate.latitude,googleCoordinate.longitude))
        }
        
        //get the area of the polygon for saving, note this returns incorrect results if polygon overlaps
        let area = GoogleMaps.GMSGeometryArea(editedPath)
        
        //calculate central point for saving
        //note: here we're calculating the centre of the bounding rectange as the "centre" of the polygon
        //this is good enough for most polygons if the aim is just to find a reasonable coordinate to display the area value
        //this will not work well for some polygons, i.e. for triangular shapes it will be off centre but still within the polygon area
        //this will fail badly for concave shaped polygons i.e. polgons shaped like the letter "L", it will be outside the polygon area
        //for triangular polygons we could work out the centroid point, this will be significantly more work
        //for concave polygons we could work out some kind of visual "centre", but this might not always be meaningful
        let bounds = GMSCoordinateBounds(path: editedPath)
        let centreLongtitude = (bounds.southWest.longitude + bounds.northEast.longitude) * 0.5
        let centreLatitude = (bounds.southWest.latitude + bounds.northEast.latitude) * 0.5
        let centre = (centreLatitude,centreLongtitude)
        
        
        //save polygon data
        presenter?.savePolygon(pathLatitudeLongitude: rawPath, area: area, centreLatitudeLongitude: centre)
        
        addCentreMakerWithArea(area: area, centreLattidue: centreLatitude, centreLongitude: centreLongtitude)
    
        clearOutCurrentEditedShapes()
    }

}

extension MainMapView: MainMapViewProtocol {
    func didGetPolygons(polygons: [Polygon]) {
        drawRetreivedPolygons(polygons: polygons)
    }
}

extension MainMapView: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        drawPolygonPoint(point: coordinate)
        editedPath.count() > 2 ? (savePolygonButton.isHidden = false) : (savePolygonButton.isHidden = true)
    }
}


extension MainMapView {
    func drawPolygonPoint(point: CLLocationCoordinate2D) {
        //add a point to the polygon path and redraw polygon
        let marker = GMSMarker(position: point)
        marker.map = mapView
        editedPath.add(point)
        editedPolygon.map = nil
        editedPolygon = GMSPolygon(path: editedPath)
        editedPolygon.strokeColor = .red
        editedPolygon.fillColor = .orange.withAlphaComponent(0.5)
        editedPolygon.strokeWidth = 1.0
        editedPolygon.map = mapView
    }
    
    func addCentreMakerWithArea(area: Double, centreLattidue: Double, centreLongitude: Double) {
        let marker = GMSMarker(position:CLLocationCoordinate2DMake(centreLattidue, centreLongitude))
        marker.icon = GMSMarker.markerImage(with: .lightGray)
        marker.title = "Area"
        marker.snippet = String(format:"%.2f", area) + " m2"
        marker.map = mapView
        mapView.selectedMarker = marker
    }
    
    func clearOutCurrentEditedShapes() {
        editedPolygon = GMSPolygon()
        editedPath.removeAllCoordinates()
    }
    
    func drawRetreivedPolygons(polygons: [Polygon]) {
        for polygon in polygons {
            let coord = polygon.coordinates as! Set<Coordinate>
            for point in coord {
                drawPolygonPoint(point:CLLocationCoordinate2D(latitude: point.latitude?.doubleValue ?? 0.0, longitude: point.longitude?.doubleValue ?? 0.0) )
            }
            
            clearOutCurrentEditedShapes()
            
            if let centre = polygon.centreCoordinate,
            let latitude = centre.latitude?.doubleValue,
            let longitude = centre.longitude?.doubleValue,
            let area = polygon.area?.doubleValue {
                addCentreMakerWithArea(area: area, centreLattidue: latitude, centreLongitude: longitude)
            }
        }
        
        //centre the view on the last user drawn polygon
        //note: this has been delayed by a second as it would not work otherwise, probably due to other animations happening to the map on start up
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {[weak self] in
            guard let self = self else { return }
            if let lastPolygonLatitude = polygons.last?.centreCoordinate?.latitude?.doubleValue,
            let lastPolygonLongitude = polygons.last?.centreCoordinate?.longitude?.doubleValue,
               !self.mapView.isHidden {
                self.mapView.animate(with: GMSCameraUpdate.setTarget(CLLocationCoordinate2DMake(lastPolygonLatitude, lastPolygonLongitude)))
                self.mapView.animate(toZoom: self.preciseLocationZoomLevel)
            }
        }
    }
}

extension MainMapView: CLLocationManagerDelegate {
    
    // Handle incoming location events.
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
      let location: CLLocation = locations.last!
        goToLocation(location: location)
    }

    // Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
      // Check accuracy authorization
      let accuracy = manager.accuracyAuthorization
      switch accuracy {
      case .fullAccuracy:
          print("Location accuracy is precise.")
      case .reducedAccuracy:
          print("Location accuracy is not precise.")
      @unknown default:
        fatalError()
      }

      // Handle authorization status
      switch status {
      case .restricted:
        print("Location access was restricted.")
      case .denied:
        print("User denied access to location.")
        // Display the map using the default location.
          mapView.isHidden = false
      case .notDetermined:
        print("Location status not determined.")
      case .authorizedAlways: fallthrough
      case .authorizedWhenInUse:
        print("Location status is OK.")
          if let location = locationManager.location {
              goToLocation(location: location)
          }
      @unknown default:
        fatalError()
      }
    }

    // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
      locationManager.stopUpdatingLocation()
      print("Error: \(error)")
    }
    
    func goToLocation(location: CLLocation) {
        let zoomLevel = locationManager.accuracyAuthorization == .fullAccuracy ? preciseLocationZoomLevel : approximateLocationZoomLevel
        let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
                                              longitude: location.coordinate.longitude,
                                              zoom: zoomLevel)

        if mapView.isHidden {
          mapView.isHidden = false
          mapView.camera = camera
        } else {
            mapView.camera = camera
          mapView.animate(to: camera)
        }
    }
  }
