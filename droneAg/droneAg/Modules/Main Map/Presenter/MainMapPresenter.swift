//
//  MainMapPresenter.swift
//  droneAg
//
//  Created by Nikolaus Banjo 07729431578 on 21/03/2022.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.

class MainMapPresenter: MainMapPresenterProtocol {
    weak var view: MainMapViewProtocol?
    var interactor: MainMapInteractorInputProtocol?
    var wireframe: MainMapWireframeProtocol?
    
    func getPolygons() {
        interactor?.getPolygons()
    }
}

extension MainMapPresenter: MainMapInteractorOutputProtocol {
    func didGetPolygons(polygons: [Polygon]) {
        view?.didGetPolygons(polygons: polygons)
    }
    
    func savePolygon(pathLatitudeLongitude: [(Double,Double)], area: Double, centreLatitudeLongitude: (Double,Double)) {
        interactor?.savePolygon(pathLatitudeLongitude: pathLatitudeLongitude, area: area, centreLatitudeLongitude: centreLatitudeLongitude)
    }
}
