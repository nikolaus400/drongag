//
//  MainMapProtocols.swift
//  droneAg
//
//  Created by Nikolaus Banjo 07729431578 on 21/03/2022.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//
import UIKit

protocol MainMapViewProtocol: AnyObject {
    func didGetPolygons(polygons: [Polygon])
}

protocol MainMapPresenterProtocol: AnyObject {
    var view: MainMapViewProtocol? { get set }
    var interactor: MainMapInteractorInputProtocol? { get set }
    var wireframe: MainMapWireframeProtocol? { get set }
    
    func getPolygons()
    func savePolygon(pathLatitudeLongitude: [(Double,Double)], area: Double, centreLatitudeLongitude: (Double,Double))
}

protocol MainMapInteractorInputProtocol: AnyObject {
    var presenter: MainMapInteractorOutputProtocol? { get set }
    func getPolygons()
    func savePolygon(pathLatitudeLongitude: [(Double,Double)], area: Double, centreLatitudeLongitude: (Double,Double))
}

protocol MainMapInteractorOutputProtocol: AnyObject {
    func didGetPolygons(polygons: [Polygon])
}

protocol MainMapWireframeProtocol: AnyObject {
    static func createMainMapModule() -> UIViewController
}
