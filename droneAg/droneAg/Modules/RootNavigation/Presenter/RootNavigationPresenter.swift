//
//  RootNavigationPresenter.swift
//  droneAg
//
//  Created by Nikolaus Banjo 07729431578 on 21/03/2022.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.

class RootNavigationPresenter: RootNavigationPresenterProtocol {
    weak var view: RootNavigationViewProtocol?
    var interactor: RootNavigationInteractorInputProtocol?
    var wireframe: RootNavigationWireframeProtocol?
    
    func showMainMap() {
        wireframe?.showMainMap(from: view!)
    }
}

extension RootNavigationPresenter: RootNavigationInteractorOutputProtocol {
    
}
