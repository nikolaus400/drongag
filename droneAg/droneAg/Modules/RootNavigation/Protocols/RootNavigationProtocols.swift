//
//  RootNavigationProtocols.swift
//  droneAg
//
//  Created by Nikolaus Banjo 07729431578 on 21/03/2022.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//
import UIKit

protocol RootNavigationViewProtocol: AnyObject {
    
}

protocol RootNavigationPresenterProtocol: AnyObject {
    var view: RootNavigationViewProtocol? { get set }
    var interactor: RootNavigationInteractorInputProtocol? { get set }
    var wireframe: RootNavigationWireframeProtocol? { get set }
    
    func showMainMap()
}

protocol RootNavigationInteractorInputProtocol: AnyObject {
    var presenter: RootNavigationInteractorOutputProtocol? { get set }
}

protocol RootNavigationInteractorOutputProtocol: AnyObject {
}

protocol RootNavigationWireframeProtocol: AnyObject {
    static func createRootNavigationModule() -> UIViewController
    
    func showMainMap(from view: RootNavigationViewProtocol)
}
