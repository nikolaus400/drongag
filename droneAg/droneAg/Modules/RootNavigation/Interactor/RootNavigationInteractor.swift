//
//  RootNavigationInteractor.swift
//  droneAg
//
//  Created by Nikolaus Banjo 07729431578 on 21/03/2022.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//

class RootNavigationInteractor: RootNavigationInteractorInputProtocol {
    weak var presenter: RootNavigationInteractorOutputProtocol?
}
