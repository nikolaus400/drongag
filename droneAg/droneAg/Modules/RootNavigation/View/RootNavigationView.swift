//
//  RootNavigationView.swift
//  droneAg
//
//  Created by Nikolaus Banjo 07729431578 on 21/03/2022.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.

import UIKit

class RootNavigationView: UINavigationController, UINavigationControllerDelegate {
    
    //MARK: - Variables
    var presenter: RootNavigationPresenterProtocol?
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpMainMap()
    }
    
    func setUpMainMap() {
        presenter?.showMainMap()
    }
}

extension RootNavigationView {
    //MARK: - IBActions

}

extension RootNavigationView: RootNavigationViewProtocol {
    
}
