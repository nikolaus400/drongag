//
//  RootNavigationWireframe.swift
//  droneAg
//
//  Created by Nikolaus Banjo 07729431578 on 21/03/2022.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//
import UIKit

class RootNavigationWireframe:  RootNavigationWireframeProtocol {
    static func createRootNavigationModule() -> UIViewController {
        guard let view = UIStoryboard(name: "RootNavigationView", bundle: nil).instantiateViewController(withIdentifier: "RootNavigationView") as? RootNavigationView else {
            return UIViewController()
        }
        let presenter: RootNavigationPresenterProtocol & RootNavigationInteractorOutputProtocol = RootNavigationPresenter()
        let interactor: RootNavigationInteractorInputProtocol = RootNavigationInteractor()
        let wireframe: RootNavigationWireframeProtocol = RootNavigationWireframe()
        
        view.presenter = presenter
        presenter.view = view
        presenter.interactor = interactor
        presenter.wireframe = wireframe
        interactor.presenter = presenter
        
        return view
    }
    
    func showMainMap(from view: RootNavigationViewProtocol) {
        guard let navView = view as? UINavigationController else {
            return
        }
        let mapView = MainMapWireframe.createMainMapModule()
        navView.setViewControllers([mapView], animated: false)
    }
    
}
