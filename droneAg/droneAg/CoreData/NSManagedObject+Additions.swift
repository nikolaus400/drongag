//
//  NSManagedObject+Additions.swift
//  droneAg
//
//  Created by Nikolaus Banjo 07729431578 on 23/03/2022.
//

import CoreData

public extension NSManagedObject {
    class func createManagedObject<T: NSManagedObject>(context: NSManagedObjectContext) -> T {
        let entityName = String(describing: self)
        guard let entityDescription = NSEntityDescription.entity(forEntityName: entityName, in: context) else { fatalError("Unable to create \(entityName) NSEntityDescription") }
        guard let object = NSManagedObject(entity: entityDescription, insertInto: context) as? T else { fatalError("Unable to create \(entityName) NSManagedObject") }
        return object
    }
}
