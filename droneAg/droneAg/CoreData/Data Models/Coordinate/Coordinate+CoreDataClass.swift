//
//  Coordinate+CoreDataClass.swift
//  droneAg
//
//  Created by Nikolaus Banjo 07729431578 on 23/03/2022.
//

import Foundation
import CoreData

@objc(Coordinate)
public class Coordinate: NSManagedObject {
    static func create(context: NSManagedObjectContext) -> Coordinate {
        return Coordinate.createManagedObject(context: context) as Coordinate
    }
}
