//
//  CustomCoordinate+CoreDataProperties.swift
//  droneAg
//
//  Created by Nikolaus Banjo 07729431578 on 23/03/2022.
//

import Foundation
import CoreData

extension Coordinate {
    @NSManaged public var latitude: NSNumber?
    @NSManaged public var longitude: NSNumber?
}


