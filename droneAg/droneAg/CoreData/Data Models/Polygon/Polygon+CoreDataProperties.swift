//
//  Polygon+CoreDataProperties.swift
//  droneAg
//
//  Created by Nikolaus Banjo 07729431578 on 23/03/2022.
//

import Foundation
import CoreData

extension Polygon {
    @NSManaged public var timeStamp: Date?
    @NSManaged public var coordinates: NSSet?
    @NSManaged public var area: NSNumber?
    @NSManaged public var centreCoordinate: Coordinate?
}

// MARK: Generated accessors for coordinates

extension Polygon {
    @objc(addCoordinatesObject:)
    @NSManaged public func addToCoordinates(_ value: Coordinate)
}
