//
//  Polygon+CoreDataClass.swift
//  droneAg
//
//  Created by Nikolaus Banjo 07729431578 on 23/03/2022.
//

import Foundation
import CoreData

@objc(Polygon)
public class Polygon: NSManagedObject {
    static func create(context: NSManagedObjectContext) -> Polygon {
        let polygon = Polygon.createManagedObject(context: context) as Polygon
        polygon.timeStamp = Date()
        return polygon
    }
    
    static func getFetchRequest() -> NSFetchRequest<Polygon> {
        let fetchRequest = NSFetchRequest<Polygon>(entityName: "Polygon")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "timeStamp", ascending: true)]
        return fetchRequest
    }
    
}
