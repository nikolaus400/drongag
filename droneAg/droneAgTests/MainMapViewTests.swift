//
//  MainMapViewTests.swift
//  droneAgTests
//
//  Created by Nikolaus Banjo 07729431578 on 25/03/2022.
//

import Foundation
import XCTest
@testable import droneAg

class MainMapViewTests: XCTestCase {
    var moduleView: MainMapView?
    let mockView = MockMainMapView()
    let mockPresenter = MockMainMapPresenter()
    let mockInteractor = MockMainMapInteractor()
    let mockWireframe = MockMainMapWireframe()

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        moduleView = MainMapWireframe.createMainMapModule() as? MainMapView

        XCTAssertNotNil(moduleView)

        _ = moduleView!.view
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func test_Module_ViperComponents_Setup() {
        XCTAssertTrue(moduleView?.presenter is MainMapPresenter)
        XCTAssertTrue(moduleView?.presenter?.view is MainMapView)
        XCTAssertTrue(moduleView?.presenter?.interactor is MainMapInteractor)
        XCTAssertTrue(moduleView?.presenter?.wireframe is MainMapWireframe)
        XCTAssertTrue(moduleView?.presenter?.interactor?.presenter is MainMapPresenter)
    }

    func test_View_Outlets_NotNil_onViewDidLoad() throws {
        let view = try XCTUnwrap(moduleView)
        XCTAssertNotNil(view.savePolygonButton)
        XCTAssertNotNil(view.mapView)
    }
    
    func test_saveButton_correctText() throws {
        let view = try XCTUnwrap(moduleView)
        XCTAssertEqual(view.savePolygonButton.titleLabel?.text, "Save Polygon")
        
    }
}

// MARK: - Mock classes

class MockMainMapView: MainMapViewProtocol {
    var presenter: MainMapPresenterProtocol?
    
    func didGetPolygons(polygons: [Polygon]) {
        
    }
}

class MockMainMapPresenter: MainMapPresenterProtocol, MainMapInteractorOutputProtocol {
    var view: MainMapViewProtocol?
    var interactor: MainMapInteractorInputProtocol?
    var wireframe: MainMapWireframeProtocol?
    
    func getPolygons() {
        
    }
    
    func savePolygon(pathLatitudeLongitude: [(Double, Double)], area: Double, centreLatitudeLongitude: (Double, Double)) {
        
    }
    
    func didGetPolygons(polygons: [Polygon]) {
        
    }
}

class MockMainMapInteractor: MainMapInteractorInputProtocol {
    var presenter: MainMapInteractorOutputProtocol?
    
    func getPolygons() {
        
    }
    
    func savePolygon(pathLatitudeLongitude: [(Double, Double)], area: Double, centreLatitudeLongitude: (Double, Double)) {
        
    }
}

class MockMainMapWireframe: MainMapWireframeProtocol {
    
    static func createMainMapModule() -> UIViewController { return UIViewController() }

}
